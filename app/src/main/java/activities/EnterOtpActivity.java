package activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.oxydeliveryboy.MainActivity;
import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.ActivityEnterOtpBinding;
import com.oxydeliveryboy.retrofit.APIClientMain;
import com.oxydeliveryboy.retrofit.APIInterface;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import modal.OTPSubmitModal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterOtpActivity extends AppCompatActivity {
    ActivityEnterOtpBinding binding;
    String number, Otpnumber, token;
    private static final String TAG = "EnterOtpActivity";
    int addressId;
    StorageUtils storageUtils;
    Context context;
    ProgressDialog progressDialog;
    private APIInterface apiInterface;
//    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_enter_otp);

        context = EnterOtpActivity.this;

        progressDialog = new ProgressDialog(EnterOtpActivity.this);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);

        number = getIntent().getStringExtra("userNumber");
//        Toast.makeText(context, "number--" + number, Toast.LENGTH_SHORT).show();
        storageUtils = new StorageUtils();


        binding.btnVerifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                Otpnumber = binding.editOtp.getText().toString();
                if (Otpnumber.isEmpty()) {
                    progressDialog.dismiss();
                    Toast.makeText(EnterOtpActivity.this, "Enter Otp!", Toast.LENGTH_SHORT).show();
                } else {
                    getPhoneNumberOtpSubmit(number, Otpnumber, storageUtils, context, progressDialog);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
//        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private BroadcastReceiver SmsListener = new BroadcastReceiver() {

        @SuppressWarnings("deprecation")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(
                    "android.provider.Telephony.SMS_RECEIVED")) {
                Bundle bundle = intent.getExtras(); // ---get the SMS message
                // passed in---
                SmsMessage[] msgs = null;
                // String msg_from;
                if (bundle != null) {
                    // ---retrieve the SMS message received---
                    try {
                        Object[] pdus = (Object[]) bundle.get("pdus");
                        msgs = new SmsMessage[pdus.length];
                        for (int i = 0; i < msgs.length; i++) {
                            msgs[i] = SmsMessage
                                    .createFromPdu((byte[]) pdus[i]);
                            // msg_from = msgs[i].getOriginatingAddress();
                            String msgBody = msgs[i].getMessageBody();
                            Toast.makeText(context, "msgBody--" + msgBody, Toast.LENGTH_SHORT).show();
                            // do your stuff
                        }
                    } catch (Exception e) {
                        // Log.d("Exception caught",e.getMessage());
                    }
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        EnterOtpActivity.this.unregisterReceiver(SmsListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter i = new IntentFilter(
                "android.provider.Telephony.SMS_RECEIVED");
        EnterOtpActivity.this.registerReceiver(SmsListener, i);
    }

    public void getPhoneNumberOtpSubmit(String number, String otp,
                                        StorageUtils storageUtils, Context context, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", "DEALBOY_" + number);
        jsonObject.addProperty("password", otp);
        Call<OTPSubmitModal> call = apiInterface.submitPhonenumberOtp(jsonObject);
        call.enqueue(new Callback<OTPSubmitModal>() {
            @Override
            public void onResponse(@NotNull Call<OTPSubmitModal> call,
                                   @NotNull Response<OTPSubmitModal> response) {
                progressDialog.dismiss();
                OTPSubmitModal detailsModal = response.body();
                if (detailsModal != null) {
                    storageUtils.setDetails(context, StringUtils.NewUser, String.valueOf(detailsModal.isIs_new_user()));
                    token = detailsModal.getKey();
//                    addressId = detailsModal.getCustomer_details().getDefault_address();
//                    int userid = detailsModal.getCustomer_details().getId();
//                    String cityName = detailsModal.getCustomer_details().get();
                      String userName = detailsModal.getCustomer_details().getName();
                      String userDob = detailsModal.getCustomer_details().getDob();
//                    String userProfile = detailsModal.getCustomer_details().();
//                    String dealerId = String.valueOf(detailsModal.getCustomer_details().getDefault_dealer());
//                    Toast.makeText(EnterOtpActivity.this, "dealerId--" + dealerId, Toast.LENGTH_SHORT).show();

                    storageUtils.setDetails(context, StringUtils.IsLogin, "Yes");
                    storageUtils.setDetails(context, StringUtils.UserToken, token);
//                    storageUtils.setDetails(context, StringUtils.DefaultAddress, String.valueOf(addressId));
//                    storageUtils.setDetails(context, StringUtils.UserId, String.valueOf(userid));
                    storageUtils.setDetails(context, StringUtils.UserName, userName);
                    storageUtils.setDetails(context, StringUtils.UserDob, userDob);
                    startActivity(new Intent(EnterOtpActivity.this, MainActivity.class));
                    finishAffinity();
                } else {

                }
            }

            @Override
            public void onFailure(@NotNull Call<OTPSubmitModal> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, t.getMessage());
            }
        });
    }
}