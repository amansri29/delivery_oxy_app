package activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.ActivityTotalRouteBinding;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import modal.CurrentToDeliveryLocation;

public class TotalRouteActivity extends AppCompatActivity implements OnMapReadyCallback {
    ActivityTotalRouteBinding binding;
    private GoogleMap googleMap;
    private ArrayList<CurrentToDeliveryLocation> locations;
    private static final String TAG = "TotalRouteActivity";
    private StorageUtils storageUtils;
    private String currentDate;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_total_route);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        locations = getIntent().getParcelableArrayListExtra("locations");
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        binding.imgBack.setOnClickListener(v -> {
            onBackPressed();
        });
        storageUtils = new StorageUtils();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = sdf.format(Calendar.getInstance().getTime());
        binding.tvGetDeliveryReRoute.setOnClickListener(v -> {
            new AlertDialog.Builder(TotalRouteActivity.this)
                    .setTitle("Attention")
                    .setMessage("This will Re-Create the route from your current Location")
                    .setPositiveButton("GOT IT", (dialogInterface, i) -> {
                        googleMap.clear();
                        storageUtils.clearMapRelatedFields(TotalRouteActivity.this);
                        requestNewLocationData();
                    })
                    .setNegativeButton("CANCEL", (dialogInterface, i) -> {

                    })
                    .show();
        });
    }


    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (checkLocationPermissions()) {
            getSavedLocationOrCurrentLocation();
        } else {
            requestLocationPermissions();
        }
    }

    private Boolean checkLocationPermissions() {
        return ContextCompat.checkSelfPermission(TotalRouteActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(TotalRouteActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (checkLocationPermissions()) {
            getSavedLocationOrCurrentLocation();
        } else {
            Toast.makeText(this, "Please allow location permission to use the app", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestLocationPermissions() {
        String[] permissionSet = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        ActivityCompat.requestPermissions(this, permissionSet, 20);
    }

    private void setCameraWithCoordinationBounds(List<com.akexorcist.googledirection.model.Route> route,
                                                 GoogleMap googleMap) {
        LatLng southwest = route.get(0).getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.get(route.size() - 1).getBound().getNortheastCoordination()
                .getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    private void plotPolyLine(LatLng positionFrom) {


        googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).position(positionFrom).title("Your Location"));

        List<LatLng> wayPoints = new ArrayList<>();

        for (CurrentToDeliveryLocation modal : locations) {
            LatLng latLng = new LatLng(modal.getLat(), modal.getLon());
            wayPoints.add(latLng);
            if (!modal.getStatus().equals("Delivered"))
                googleMap.addMarker(new MarkerOptions().position(latLng).title(modal.getBasic_address()));
            else
                googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).position(latLng).title(modal.getBasic_address()));
        }

        int waypointsLength = wayPoints.size();
        waypointsLength--;
        LatLng positionTo = new LatLng(wayPoints.get(waypointsLength).latitude, wayPoints.get(waypointsLength).longitude);
        wayPoints.remove(waypointsLength);

        Log.d(TAG, "plotPolyLine: " + positionFrom);
        Log.d(TAG, "plotPolyLine: " + positionTo);

        GoogleDirection.withServerKey("AIzaSyAtfg4ZcsMvoVsrML7SXMgnWo2KAaUwDvI")
                .from(positionFrom)
                .and(wayPoints)
                .to(positionTo)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(@Nullable Direction direction) {
                        if (direction.isOK()) {
                            List<Route> route = direction.getRouteList();
                            /*myMap.addMarker(new MarkerOptions().position(positionFrom).title("Your Location"));
                            myMap.addMarker(new MarkerOptions().position(positionTo).title("Deliver Here"));*/
                            Log.d(TAG, "onDirectionSuccess: " + route.size());
                            Log.d(TAG, "onDirectionSuccess: " + route.get(0).getLegList().size());
                            ArrayList<LatLng> directionPositionList = new ArrayList<>();
                            for (Leg obj : route.get(0).getLegList()) {
                                directionPositionList.addAll(obj.getDirectionPoint());
                            }
                            googleMap.addPolyline(DirectionConverter.createPolyline(
                                    TotalRouteActivity.this,
                                    directionPositionList,
                                    3,
                                    Color.BLACK
                            ));
                            /*googleMap.addPolyline(DirectionConverter.createPolyline(
                                    TotalRouteActivity.this,
                                    directionPositionList1,
                                    3,
                                    Color.BLACK
                            ));*/
                            setCameraWithCoordinationBounds(route, googleMap);
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionTo, 12));
                            Log.d(TAG, "route Success: ");
                        } else {
                            // Do something
                            Log.d(TAG, "onDirectionFailure:else " + direction.getErrorMessage());
                        }
                    }

                    @Override
                    public void onDirectionFailure(@NonNull Throwable t) {
                        Log.d(TAG, "onDirectionFailure: " + t.getMessage());
                    }
                });

    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            storageUtils.setDetails(TotalRouteActivity.this, StringUtils.CurrentLocFetchDate, currentDate);
            storageUtils.setDetails(TotalRouteActivity.this, StringUtils.CurrentLocLat, String.valueOf(mLastLocation.getLatitude()));
            storageUtils.setDetails(TotalRouteActivity.this, StringUtils.CurrentLocLong, String.valueOf(mLastLocation.getLongitude()));
            plotPolyLine(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
            mFusedLocationClient.removeLocationUpdates(this);
        }
    };

    private void getSavedLocationOrCurrentLocation() {
        String currentLocDate = storageUtils.getDetails(this, StringUtils.CurrentLocFetchDate);
        LatLng positionFrom;
        if (currentLocDate.equals(currentDate)) {
            double lat = Double.parseDouble(storageUtils.getDetails(this, StringUtils.CurrentLocLat));
            double lon = Double.parseDouble(storageUtils.getDetails(this, StringUtils.CurrentLocLong));
            positionFrom = new LatLng(lat, lon);
            plotPolyLine(positionFrom);
        } else {
            requestNewLocationData();
        }
    }

}