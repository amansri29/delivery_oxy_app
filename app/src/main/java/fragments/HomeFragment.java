package fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.FragmentHomeBinding;
import com.oxydeliveryboy.retrofit.APIClientMain;
import com.oxydeliveryboy.retrofit.APIInterface;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import activities.TotalRouteActivity;
import adapters.DeliveryDateAdapter;
import adapters.HomeFragAdapter;
import im.delight.android.location.SimpleLocation;
import listeners.DealersCallListeners;
import listeners.GetSelectedDeliveryDateListener;
import modal.AllOrdersGetModal;
import modal.CurrentToDeliveryLocation;
import modal.DeliveryDateModal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    FragmentHomeBinding binding;
    HomeFragAdapter homeFragAdapter;
    //    ArrayList<AllOrderModal> dealersModalArrayList;
    String token, finalDate = "", currentDate, tomorrowDate, selectedDeliverydate = "", amount;
    StorageUtils storageUtils;
    Context context;
    private APIInterface apiInterface;
    ArrayList<DeliveryDateModal> arrayList;
    DeliveryDateAdapter deliveryDateAdapter;
    List<AllOrdersGetModal> allDealersModals = new ArrayList<>();
    List<AllOrdersGetModal> allOrders;
    private ProgressDialog progressDialog;
    private SimpleLocation simpleLocation;
    private String today = "";
    private String currentLat, currentLong;
    private int adapterPosition = -1;
    private Parcelable mListState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_home, container, false));
        context = getActivity();
        storageUtils = new StorageUtils();


        arrayList = new ArrayList<>();
        token = storageUtils.getDetails(context, StringUtils.UserToken);

        currentDay();
        nextDay();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM");
        SimpleDateFormat sdfullFormat = new SimpleDateFormat("yyyy-MM-dd ");
        for (int i = 0; i < 7; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf.format(calendar.getTime());
            String fulldate = sdfullFormat.format(calendar.getTime());
            arrayList.add(new DeliveryDateModal(day, fulldate));
        }

        binding.dateTabsLayout.setHasFixedSize(true);
        deliveryDateAdapter = new DeliveryDateAdapter(getActivity(), arrayList, currentDate, tomorrowDate,
                new GetSelectedDeliveryDateListener() {
                    @Override
                    public void getDeliveryDate(int position) {
                        selectedDeliverydate = arrayList.get(position).getDeliveryFulldateName();
                        storageUtils.setDetails(getContext(), StringUtils.DeliveryDate, selectedDeliverydate.trim());
                        getDateWiseOrders(token, selectedDeliverydate);
                        if (position > 0) {
                            binding.layoutGetDelivery.setVisibility(View.GONE);
                        } else {
                            binding.layoutGetDelivery.setVisibility(View.VISIBLE);
                        }
                    }
                });
        binding.dateTabsLayout.setAdapter(deliveryDateAdapter);

        binding.tvGetDeliveryRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allOrders != null && allOrders.size() > 0) {

                    ArrayList<CurrentToDeliveryLocation> latlongs = new ArrayList<>();
                    for (AllOrdersGetModal model : allOrders) {
                        latlongs.add(new CurrentToDeliveryLocation(model.getAddress_details().getLatitude(), model.getAddress_details().getLongitude(), model.getStatus(), model.getAddress_details().getBasic_address()));
                    }
                    startActivity(
                            new Intent(getActivity(), TotalRouteActivity.class)
                                    .putParcelableArrayListExtra("locations", latlongs)
                    );
                }
            }
        });

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        //getAllOrders(token, progressDialog);
        selectedDeliverydate = arrayList.get(0).getDeliveryFulldateName();
        //getDateWiseOrders(token, progressDialog, selectedDeliverydate);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        simpleLocation = new SimpleLocation(requireContext());
        today = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        if (today.equals(storageUtils.getDetails(requireContext(), StringUtils.CurrentLocFetchDate))) {
            currentLat = storageUtils.getDetails(requireContext(), StringUtils.CurrentLocLat);
            currentLong = storageUtils.getDetails(requireContext(), StringUtils.CurrentLocLong);
        } else {
            currentLat = String.valueOf(simpleLocation.getLatitude());
            currentLong = String.valueOf(simpleLocation.getLongitude());
            storageUtils.setDetails(requireContext(), StringUtils.CurrentLocLat, currentLat);
            storageUtils.setDetails(requireContext(), StringUtils.CurrentLocLong, currentLong);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDateWiseOrders(token, selectedDeliverydate);
    }

    private void getAllOrders(String token, ProgressDialog progressDialog) {

        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getAllorders(Token);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                allDealersModals = response.body();


                if (allDealersModals != null) {
                    homeFragAdapter = new HomeFragAdapter(getActivity(), allDealersModals,
                            new DealersCallListeners() {
                                @Override
                                public void dealersCall(int position) {
                                    Toast.makeText(context, "Call to the delar", Toast.LENGTH_SHORT).show();
//                                    String number = allDealersModals.get(position).get();
//                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                                    callIntent.setData(Uri.parse("tel:" + number));
//                                    startActivity(callIntent);
                                }
                            });

                    binding.allOrderRecycler.setAdapter(homeFragAdapter);
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getDateWiseOrders(String token, String selectedDeliverydate) {

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getOrdersAtParticularDay(Token, selectedDeliverydate.trim());
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {

                allOrders = response.body();
                allDealersModals.clear();
                for (AllOrdersGetModal obj : allOrders) {
                    int security_deposit = 0;
                    obj.setItemWithSecurity(new ArrayList<>());
                    if (!obj.getStatus().equals("Delivered") && !obj.getStatus().equals("Returned")) {
                        obj.calculateDistance(Double.parseDouble(currentLat), Double.parseDouble(currentLong));
                        allDealersModals.add(obj);
                    }
                    for(AllOrdersGetModal.ItemsBean item : obj.getItems()){
                        if(item.getSecurity_deposit() > 0){
                            obj.getItemWithSecurity().add(new AllOrdersGetModal.ItemsBean(item));
                            security_deposit = security_deposit + item.getSecurity_deposit() * item.getQuantity();
                        }
                    }
                    obj.setTotalSecurityDeposit(security_deposit);

                }
                Collections.sort(allDealersModals, (o1, o2) -> (int) (o1.getDeliveryDistance() - o2.getDeliveryDistance()));

                if (allDealersModals != null) {
                    homeFragAdapter = new HomeFragAdapter(getActivity(), allDealersModals,
                            new DealersCallListeners() {
                                @Override
                                public void dealersCall(int position) {
                                    Toast.makeText(context, "Call to the delar", Toast.LENGTH_SHORT).show();
//                                    String number = allDealersModals.get(position).get();
//                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                                    callIntent.setData(Uri.parse("tel:" + number));
//                                    startActivity(callIntent);
                                }
                            });

                    binding.allOrderRecycler.setAdapter(homeFragAdapter);
                    int totalCount = homeFragAdapter.getItemCount();
                    if (totalCount == 0) {
                        binding.tvNoOrder.setVisibility(View.VISIBLE);
                        binding.allOrderRecycler.setVisibility(View.GONE);
                        binding.layoutGetDelivery.setVisibility(View.GONE);
                    } else {
                        binding.tvNoOrder.setVisibility(View.GONE);
                        binding.allOrderRecycler.setVisibility(View.VISIBLE);
                        binding.layoutGetDelivery.setVisibility(View.VISIBLE);
                    }
                    try {
                        binding.allOrderRecycler.getLayoutManager().onRestoreInstanceState(mListState);
                       /* if (adapterPosition != -1) {
                            binding.allOrderRecycler.scrollToPosition(adapterPosition);
                        }*/
                    } catch (Exception e) {

                    }
                    progressDialog.dismiss();
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void currentDay() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM", Locale.getDefault());
        currentDate = df.format(c);
//        Toast.makeText(context, "currentdate-=-" + currentDate, Toast.LENGTH_SHORT).show();
    }

    public void nextDay() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM", Locale.getDefault());
        tomorrowDate = df.format(tomorrow);
//        Toast.makeText(context, "tomorrowDate-=-" + tomorrowDate, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            LinearLayoutManager layoutManager = (LinearLayoutManager) binding.allOrderRecycler.getLayoutManager();
            mListState = layoutManager.onSaveInstanceState();
            outState.putParcelable("LIST_STATE",mListState);
            adapterPosition = layoutManager.findLastCompletelyVisibleItemPosition();
        }catch (Exception e){

        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null){
            mListState = savedInstanceState.getParcelable("LIST_STATE");
        }
    }
}