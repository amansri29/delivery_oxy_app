package fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;
import com.oxydeliveryboy.MainActivity;
import com.oxydeliveryboy.R;
import com.oxydeliveryboy.RouteForTodayActivity;
import com.oxydeliveryboy.databinding.FragmentTrackDeliveryBoyBinding;
import com.oxydeliveryboy.databinding.OtpBottonSheetLayoutBinding;
import com.oxydeliveryboy.databinding.OxygenCylinderNumberLayoutBinding;
import com.oxydeliveryboy.retrofit.APIClientMain;
import com.oxydeliveryboy.retrofit.APIInterface;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import modal.AllOrdersGetModal;
import modal.OrderDeliverd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackDeliveryBoyFragment extends Fragment {
    FragmentTrackDeliveryBoyBinding binding;
    private Context context;
    String destilat, destilang, sourseLat, sourseLong;
    private GoogleMap myMap;
    String totalAmount, token, otp, cylinderNumber;
    List<AllOrdersGetModal.ItemsBean> items;
    private APIInterface apiInterface;
    StorageUtils storageUtils;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            myMap = googleMap;

            GoogleDirection.withServerKey("AIzaSyAtfg4ZcsMvoVsrML7SXMgnWo2KAaUwDvI")
                    .from(new LatLng(26.9054, 75.8257))
                    .to(new LatLng(28.644800, 77.216721))
                    .avoid(AvoidType.FERRIES)
                    .avoid(AvoidType.HIGHWAYS)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(@Nullable Direction direction) {
                            if (direction.isOK()) {
                                List<Route> route = direction.getRouteList();
                                googleMap.addMarker(new MarkerOptions().position(new LatLng(26.9054,
                                        75.8257)));
                                googleMap.addMarker(new MarkerOptions().position(new LatLng(28.644800,
                                        77.216721)));

                                ArrayList<LatLng> directionPositionList =
                                        route.get(0).getLegList().get(0).getDirectionPoint();
                                googleMap.addPolyline(DirectionConverter.createPolyline(
                                        context,
                                        directionPositionList,
                                        3,
                                        Color.BLACK
                                ));
                                setCameraWithCoordinationBounds(route, googleMap);
                                myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(26.9054, 75.8257), 10));
                            } else {
                                // Do something
                            }
                        }

                        @Override
                        public void onDirectionFailure(@NonNull Throwable t) {

                        }
                    });
        }
    };


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_track_delivery_boy, container,
                false));
        context = getActivity();
        storageUtils = new StorageUtils();

//        totalAmount = getIntent().getStringExtra("totalAmount");

        binding.tvTotalAmount.setText("₹ " + totalAmount);
        token = storageUtils.getDetails(context, StringUtils.UserToken);

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        binding.tvGetDeliveryRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBottomSheet();
            }
        });

        return binding.getRoot();
    }

    private void setCameraWithCoordinationBounds(List<com.akexorcist.googledirection.model.Route> route,
                                                 GoogleMap googleMap) {

        LatLng southwest = route.get(0).getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.get(route.size() - 1).getBound().getNortheastCoordination()
                .getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    public void openBottomSheet() {
        final OtpBottonSheetLayoutBinding binding =
                DataBindingUtil.bind(getLayoutInflater().
                        inflate(R.layout.otp_botton_sheet_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context,
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        binding.tvVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otp = binding.editOtp.getText().toString().trim();
                if (otp.isEmpty()) {
                    Toast.makeText(context, "Please enter the Otp", Toast.LENGTH_SHORT).show();
                } else if (otp.length() < 4) {
                    Toast.makeText(context, "Please enter the valid Otp!", Toast.LENGTH_SHORT).show();
                } else {
                    bottomSheetDialog.dismiss();
                    openCylinderNumberBottomSheet();
                }
            }
        });

        bottomSheetDialog.show();

    }

    private void openCylinderNumberBottomSheet() {
        final OxygenCylinderNumberLayoutBinding binding =
                DataBindingUtil.bind(getLayoutInflater().
                        inflate(R.layout.oxygen_cylinder_number_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context,
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cylinderNumber = binding.editCylinderNumber.getText().toString().trim();
                if (cylinderNumber.isEmpty()) {
                    Toast.makeText(context, "Please enter the cylinder number!", Toast.LENGTH_SHORT).show();
                } else {
                    bottomSheetDialog.dismiss();
                    ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Loading.....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    orderDeliverd(progressDialog);
                }
            }
        });

        bottomSheetDialog.show();

    }

    private void orderDeliverd(ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("otp", otp);
        jsonObject.addProperty("cylender_number", cylinderNumber);
        String Token = "Token " + token;
        Call<OrderDeliverd> call = apiInterface.orderDeliverd(Token, jsonObject);
        call.enqueue(new Callback<OrderDeliverd>() {
            @Override
            public void onResponse(@NotNull Call<OrderDeliverd> call,
                                   @NotNull Response<OrderDeliverd> response) {
                progressDialog.dismiss();
                OrderDeliverd allDealersModals = response.body();
                if (allDealersModals != null) {
                    startActivity(new Intent(context, MainActivity.class));
                    getActivity().finishAffinity();
                    Toast.makeText(context, "Order Delivered successfully!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderDeliverd> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}