package modal;

import java.text.SimpleDateFormat;

public class DeliveryDateModal {
    private String deliverydateName;
    private String deliveryFulldateName;

    public DeliveryDateModal() {

    }

    public DeliveryDateModal(String deliverydateName, String deliveryFulldateName) {
        this.deliverydateName = deliverydateName;
        this.deliveryFulldateName = deliveryFulldateName;
    }

    public String getDeliveryFulldateName() {
        return deliveryFulldateName;
    }

    public void setDeliveryFulldateName(String deliveryFulldateName) {
        this.deliveryFulldateName = deliveryFulldateName;
    }

    public String getDeliverydateName() {
        return deliverydateName;
    }

    public void setDeliverydateName(String deliverydateName) {
        this.deliverydateName = deliverydateName;
    }
}
