package fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.FragmentProfileBinding;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ProfileFragment extends Fragment {
    FragmentProfileBinding binding;
    StorageUtils storageUtils;
    String userName, userDob, userAddress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_profile, container, false));

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        storageUtils = new StorageUtils();
        binding.editName.setText(storageUtils.getDetails(requireContext(), StringUtils.UserName));
        userDob = storageUtils.getDetails(requireContext(), StringUtils.UserDob);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        SimpleDateFormat dateOfBirthFormat = new SimpleDateFormat("dd/MMM/yyyy");
        try{
            Date d = format.parse(userDob);
            binding.editDob.setText(dateOfBirthFormat.format(d));
        }
        catch (Exception e){

        }
    }
}