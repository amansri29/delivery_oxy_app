package fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.BottomsheetfilterLayoutBinding;
import com.oxydeliveryboy.databinding.DatepickerBottomsheetBinding;
import com.oxydeliveryboy.databinding.FragmentCashCollectionBinding;
import com.oxydeliveryboy.retrofit.APIClientMain;
import com.oxydeliveryboy.retrofit.APIInterface;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import adapters.CashCollectionAdapter;
import listeners.BottomsheetSelectedItemListener;
import modal.AllOrdersGetModal;
import modals.AllOrderModal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CashCollectionFragment extends Fragment {
    FragmentCashCollectionBinding binding;
    private static final String TAG = "CashCollectionFragment";
    CashCollectionAdapter cashCollectionAdapter;
    private APIInterface apiInterface;
    ArrayList<AllOrderModal> dealersModalArrayList;
    String token, currentDate, finalCurrentDate, sevenDayBeforeDate,
            firstaRangeDate,
            lastRangedate, firstDateOFPrevMonth, lastDateOFPrevMonth;
    int itemCount;
    StorageUtils storageUtils;
    Context context;
    String selectedString;
    BottomsheetSelectedItemListener bottomsheetSelectedItemListener;
    Date firstDate, lastDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.bind(inflater.inflate(R.layout.fragment_cash_collection, container, false));
        context = getActivity();
        storageUtils = new StorageUtils();

        token = storageUtils.getDetails(context, StringUtils.UserToken);

        dealersModalArrayList = new ArrayList<>();

        bottomsheetSelectedItemListener = new BottomsheetSelectedItemListener() {
            @Override
            public void bottomsheetSelectedItem(String name) {
                binding.tvSelectedFilter.setText(name);
            }
        };


        binding.filterLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "Filter Clicked", Toast.LENGTH_SHORT).show();
                boottomSheetfilter();
            }
        });

        //currentDay();
        getSevenDaysBeforeDate();
//        getSevenDaysBeforeDate();
//        getPreviousMonthDate();

        return binding.getRoot();
    }

    private void boottomSheetfilter() {
        final BottomsheetfilterLayoutBinding binding =
                DataBindingUtil.bind(((FragmentActivity) getActivity()).getLayoutInflater().
                        inflate(R.layout.bottomsheetfilter_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity(),
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        /*binding.tvAllTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvAllTime.getText().toString();
//                Toast.makeText(getContext(), "selected string" + selectedString, Toast.LENGTH_SHORT).show();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();
                currentDay();
            }
        });*/
        binding.tvLastSevenDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvLastSevenDays.getText().toString();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();

                getSevenDaysBeforeDate();

            }
        });
        binding.tvLastMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedString = binding.tvLastMonth.getText().toString();
                bottomsheetSelectedItemListener.bottomsheetSelectedItem(selectedString);
                bottomSheetDialog.dismiss();

                getPreviousMonthDate();
            }
        });

        binding.tvCustomDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                final DatepickerBottomsheetBinding binding =
                        DataBindingUtil.bind(((FragmentActivity) getActivity()).getLayoutInflater().
                                inflate(R.layout.datepicker_bottomsheet,
                                        null));
                final BottomSheetDialog custombottomSheetDialog = new BottomSheetDialog(getActivity(),
                        R.style.BottomSheetDialog);

                assert binding != null;
                binding.imgBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        custombottomSheetDialog.dismiss();
                    }
                });

                binding.btnApply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        int day = binding.startDatePicker.getDayOfMonth();
                        int month = binding.startDatePicker.getMonth();
                        int year = binding.startDatePicker.getYear() - 1900;
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        Date d = new Date(year, month, day);
                        firstaRangeDate = dateFormatter.format(d);

                        int day1 = binding.endDatePicker.getDayOfMonth();
                        int month1 = binding.endDatePicker.getMonth();
                        int year1 = binding.endDatePicker.getYear() - 1900;
                        SimpleDateFormat dateFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
                        Date d1 = new Date(year1, month1, day1);
                        lastRangedate = dateFormatter1.format(d1);

                        String finalRangeDate = firstaRangeDate + " to " + lastRangedate;

                        bottomsheetSelectedItemListener.bottomsheetSelectedItem(finalRangeDate);

                        Log.d(TAG, "onClick: " + lastRangedate);

                        ProgressDialog progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setTitle("Loading.....");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        getAllCashCollectorOrders(token, progressDialog, lastRangedate, firstaRangeDate);

                        custombottomSheetDialog.dismiss();
                    }
                });
                assert binding != null;
                custombottomSheetDialog.setContentView(binding.getRoot());
                custombottomSheetDialog.show();
            }
        });

        bottomSheetDialog.show();
    }


    private void getAllCashCollectorOrders(String token, ProgressDialog progressDialog, String fDate, String sDate) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        try {
            firstDate = dateFormat.parse(sDate);
            lastDate = dateFormat.parse(fDate);
        } catch (Exception e) {

        }

        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getAllCashCollector(Token, fDate, sDate);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<AllOrdersGetModal> allDealersModals = response.body();
                    List<AllOrdersGetModal> pickUpOrders = new ArrayList<>();
                    ArrayList<AllOrdersGetModal.ItemsBean> itemsWithSecurity = new ArrayList<>();
                    int totalCollection = 0;
                    for (AllOrdersGetModal order : allDealersModals) {
                        totalCollection = totalCollection + order.getTotal_amount();
                        if (order.getReturnDate() != null) {
                            try {
                                AllOrdersGetModal orderClone = new AllOrdersGetModal(order);
                                Date returnDate = dateFormat.parse(order.getReturnDate());
                                if ((returnDate.after(firstDate) || returnDate.equals(firstDate)) && (returnDate.before(lastDate) || returnDate.equals(lastDate)) && orderClone.getPayment_mode().equals("Cash on delivery")) {
                                    orderClone.setPickupCodOrder(true);
                                    itemsWithSecurity.clear();
                                    int security_amount = 0;
                                    for (AllOrdersGetModal.ItemsBean item : orderClone.getItems()) {
                                        if (item.getSecurity_deposit() > 0) {
                                            itemsWithSecurity.add(item);
                                            security_amount = security_amount + item.getSecurity_deposit() * item.getQuantity();
                                        }
                                    }
                                    orderClone.setItemWithSecurity(itemsWithSecurity);
                                    orderClone.setTotalSecurityDeposit(security_amount);
                                    totalCollection = totalCollection - security_amount;
                                    Log.d(TAG, "onResponse: "+ totalCollection);
                                    pickUpOrders.add(orderClone);
                                }
                            } catch (Exception e) {
                                Log.d(TAG, "onResponse: "+e.getMessage() + e.getCause());
                            }
                        }

                    }
                    if(totalCollection >= 0){
                        binding.tvTotalAmount.setText("₹ " + totalCollection);
                        binding.tvTotalAmount.setTextColor(ContextCompat.getColor(context, R.color.color1F7818));
                    }
                    else{
                        binding.tvTotalAmount.setText("-₹ " + totalCollection);
                        binding.tvTotalAmount.setTextColor(ContextCompat.getColor(context, R.color.colorFE2165));
                    }
                    allDealersModals.addAll(0,pickUpOrders);
                    if (allDealersModals != null) {
                        cashCollectionAdapter = new CashCollectionAdapter(getActivity(), allDealersModals);
                        binding.recyclerCashCollection.setHasFixedSize(true);
                        binding.recyclerCashCollection.setAdapter(cashCollectionAdapter);
                        itemCount = cashCollectionAdapter.getItemCount();
                        if (itemCount == 0) {
                            binding.tvNoOrders.setVisibility(View.VISIBLE);
                        } else {
                            binding.tvNoOrders.setVisibility(View.GONE);

                        }
                    } else {
                        Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

   /* private void getLastSevenDaysOrders(String token, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getLastSevenDaysCashCollector(Token,
                currentDate, sevenDayBeforeDate);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                List<AllOrdersGetModal> allDealersModals = response.body();

                if (allDealersModals != null) {
                    cashCollectionAdapter = new CashCollectionAdapter(getActivity(), allDealersModals);
                    binding.recyclerCashCollection.setHasFixedSize(true);
                    binding.recyclerCashCollection.setAdapter(cashCollectionAdapter);
                    itemCount = cashCollectionAdapter.getItemCount();
                    if (itemCount == 0) {
                        binding.tvNoOrders.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvNoOrders.setVisibility(View.GONE);

                    }
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getLastMonthOrders(String token, ProgressDialog progressDialog) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getLastmonthCashCollector(Token,
                firstDateOFPrevMonth, lastDateOFPrevMonth);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                List<AllOrdersGetModal> allDealersModals = response.body();

                if (allDealersModals != null) {
                    cashCollectionAdapter = new CashCollectionAdapter(getActivity(), allDealersModals);
                    binding.recyclerCashCollection.setHasFixedSize(true);
                    binding.recyclerCashCollection.setAdapter(cashCollectionAdapter);
                    itemCount = cashCollectionAdapter.getItemCount();
                    if (itemCount == 0) {
                        binding.tvNoOrders.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvNoOrders.setVisibility(View.GONE);

                    }
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getRangeDateOrders(String token, ProgressDialog progressDialog, String fRDate, String lRDate) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        String Token = "Token " + token;
        Call<List<AllOrdersGetModal>> call = apiInterface.getRangDateCashCollector(Token,
                fRDate, lRDate);
        call.enqueue(new Callback<List<AllOrdersGetModal>>() {
            @Override
            public void onResponse(@NotNull Call<List<AllOrdersGetModal>> call,
                                   @NotNull Response<List<AllOrdersGetModal>> response) {
                progressDialog.dismiss();
                List<AllOrdersGetModal> allDealersModals = response.body();

                if (allDealersModals != null) {
                    cashCollectionAdapter = new CashCollectionAdapter(getActivity(), allDealersModals);
                    binding.recyclerCashCollection.setHasFixedSize(true);
                    binding.recyclerCashCollection.setAdapter(cashCollectionAdapter);
                    itemCount = cashCollectionAdapter.getItemCount();
                    if (itemCount == 0) {
                        binding.tvNoOrders.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvNoOrders.setVisibility(View.GONE);

                    }
                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<AllOrdersGetModal>> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "Failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
*/

    public void currentDay() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        SimpleDateFormat passDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        currentDate = df.format(c);
        finalCurrentDate = passDate.format(c);
        Log.d(TAG, "currentDay: " + finalCurrentDate);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        getAllCashCollectorOrders(token, progressDialog, currentDate, currentDate);

//        Toast.makeText(getActivity(), "finalCurrentDate--" + finalCurrentDate, Toast.LENGTH_SHORT).show();
    }

    public void getSevenDaysBeforeDate() {
        Calendar cal = GregorianCalendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        cal.setTime(new Date());
        currentDate = df.format(cal.getTime());
        cal.add(Calendar.DAY_OF_YEAR, -7);
        Date daysBeforeDate = cal.getTime();

        sevenDayBeforeDate = df.format(daysBeforeDate);
//        Toast.makeText(getActivity(), "sevenDayBeforeDate--" + sevenDayBeforeDate, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "currentDay: " + sevenDayBeforeDate);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        getAllCashCollectorOrders(token, progressDialog, currentDate, sevenDayBeforeDate);

    }

    private void getPreviousMonthDate() {
        Calendar aCalendar = Calendar.getInstance();
        aCalendar.set(Calendar.DATE, 1);
        aCalendar.add(Calendar.DAY_OF_MONTH, -1);
        Date lastDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        lastDateOFPrevMonth = df.format(lastDateOfPreviousMonth);
//        Toast.makeText(getContext(), "lastDateOFPrevMonth of prev month --" + lastDateOFPrevMonth, Toast.LENGTH_SHORT).show();

        aCalendar.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = aCalendar.getTime();
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        firstDateOFPrevMonth = df1.format(firstDateOfPreviousMonth);
//        Toast.makeText(getContext(), "firstDateOFPrevMonth of prev month --" + firstDateOFPrevMonth, Toast.LENGTH_SHORT).show();

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        getAllCashCollectorOrders(token, progressDialog, lastDateOFPrevMonth, firstDateOFPrevMonth);

    }
}