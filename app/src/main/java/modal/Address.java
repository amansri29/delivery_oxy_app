package modal;
public class Address {
    private String basic_address;
    private String landmark;
    private double latitude, longitude;

    public String getBasic_address() {
        return basic_address;
    }

    public String getLandmark() {
        return landmark;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}