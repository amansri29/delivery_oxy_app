package modal;

import android.os.Parcel;
import android.os.Parcelable;

public class CurrentToDeliveryLocation implements Parcelable {
    private String status;
    private String basic_address;
    private double lat;
    private double lon;
    double distance;

    public CurrentToDeliveryLocation(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }
    public CurrentToDeliveryLocation(double lat, double lon,String status, String basic_address) {
        this.lat = lat;
        this.lon = lon;
        this.status = status;
        this.basic_address = basic_address;
    }


    protected CurrentToDeliveryLocation(Parcel in) {
        lat = in.readDouble();
        lon = in.readDouble();
        distance = in.readDouble();
        status = in.readString();
        basic_address = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lon);
        dest.writeDouble(distance);
        dest.writeString(status);
        dest.writeString(basic_address);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CurrentToDeliveryLocation> CREATOR = new Creator<CurrentToDeliveryLocation>() {
        @Override
        public CurrentToDeliveryLocation createFromParcel(Parcel in) {
            return new CurrentToDeliveryLocation(in);
        }

        @Override
        public CurrentToDeliveryLocation[] newArray(int size) {
            return new CurrentToDeliveryLocation[size];
        }
    };

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void calculateDistance(double lat1, double lon1) {
        double lon, lat;
        lon1 = Math.toRadians(lon1);
        lon = Math.toRadians(this.lon);
        lat1 = Math.toRadians(lat1);
        lat = Math.toRadians(this.lat);

        // Haversine formula
        double dlon = lon - lon1;
        double dlat = lat - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat)
                * Math.pow(Math.sin(dlon / 2), 2);

        double c = 2 * Math.asin(Math.sqrt(a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;

        // calculate the result
        distance = (c * r);
    }

    public String getBasic_address() {
        return basic_address;
    }

    public String getStatus() {
        return status;
    }
}
