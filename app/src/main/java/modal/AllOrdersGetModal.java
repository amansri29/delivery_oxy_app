package modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AllOrdersGetModal implements Serializable {

    /**
     * id : 105
     * order_customer : 82
     * items : [{"id":106,"product_name":"Mask","created":"2021-07-10T18:20:14.791272+05:30","last_updated":"2021-07-10T18:20:14.791301+05:30","item_name":"standard","amount":120,"security_deposit":0,"quantity":2,"item":33},{"id":107,"product_name":"Water Bottel","created":"2021-07-10T18:20:14.801575+05:30","last_updated":"2021-07-10T18:20:14.801604+05:30","item_name":"standard","amount":130,"security_deposit":0,"quantity":1,"item":34}]
     * total_amount : 150
     * order_type : New
     * expected_delivery_date : 2020-10-10
     * repeat_order_days : 0
     * dealer_name :
     * repeat_till : 2020-10-12
     * delivery_address : null
     * payment_mode : Cash on delivery
     * status : Payment Pending
     * payment_token : null
     * customer_name : DEEPENDRA SINGH
     */

    private int id;
    private int order_customer;
    private int total_amount;
    private String order_type;
    private String expected_delivery_date;
    private int repeat_order_days;
    private String dealer_name;
    private String repeat_till;
    private String delivery_address;
    private String payment_mode;
    private String status;
    private Object payment_token;
    private String customer_name;
    private ArrayList<ItemsBean> items;
    private Address address_details;
    private double deliveryDistance;
    private String customer_contact;
    @Expose(serialize = false, deserialize = false)
    private ArrayList<ItemsBean> itemWithSecurity;
    @Expose(serialize = false, deserialize = false)
    private int totalSecurityDeposit;
    @SerializedName("return_date")
    private String returnDate;
    private String delivery_code;

    @Expose(serialize = false, deserialize = false)
    private boolean isPickupCodOrder;

    public AllOrdersGetModal(AllOrdersGetModal obj) {
        this.id = obj.id;
        this.order_customer = obj.order_customer;
        this.total_amount = obj.total_amount;
        this.order_type = obj.order_type;
        this.expected_delivery_date = obj.expected_delivery_date;
        this.repeat_order_days = obj.repeat_order_days;
        this.dealer_name = obj.dealer_name;
        this.repeat_till = obj.repeat_till;
        this.delivery_address = obj.delivery_address;
        this.payment_mode = obj.payment_mode;
        this.status = obj.status;
        this.payment_token = obj.payment_token;
        this.customer_name = obj.customer_name;
        this.items = obj.items;
        this.address_details = obj.address_details;
        this.deliveryDistance = obj.deliveryDistance;
        this.itemWithSecurity = obj.itemWithSecurity;
        this.totalSecurityDeposit = obj.totalSecurityDeposit;
        this.returnDate = obj.returnDate;
        this.isPickupCodOrder = obj.isPickupCodOrder;
    }

    public Address getAddress_details() {
        return address_details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_customer() {
        return order_customer;
    }

    public void setOrder_customer(int order_customer) {
        this.order_customer = order_customer;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public int getRepeat_order_days() {
        return repeat_order_days;
    }

    public void setRepeat_order_days(int repeat_order_days) {
        this.repeat_order_days = repeat_order_days;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getRepeat_till() {
        return repeat_till;
    }

    public void setRepeat_till(String repeat_till) {
        this.repeat_till = repeat_till;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(Object payment_token) {
        this.payment_token = payment_token;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public ArrayList<ItemsBean> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemsBean> items) {
        this.items = items;
    }

    public double getDeliveryDistance() {
        return deliveryDistance;
    }

    public ArrayList<ItemsBean> getItemWithSecurity() {
        return itemWithSecurity;
    }

    public void setItemWithSecurity(ArrayList<ItemsBean> itemWithSecurity) {
        this.itemWithSecurity = itemWithSecurity;
    }

    public int getTotalSecurityDeposit() {
        return totalSecurityDeposit;
    }

    public void setTotalSecurityDeposit(int totalSecurityDeposit) {
        this.totalSecurityDeposit = totalSecurityDeposit;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public boolean isPickupCodOrder() {
        return isPickupCodOrder;
    }

    public void setPickupCodOrder(boolean pickupCodOrder) {
        isPickupCodOrder = pickupCodOrder;
    }

    public String getCustomer_contact() {
        return customer_contact;
    }

    public String getDelivery_code() {
        return delivery_code;
    }

    public void calculateDistance(double lat1, double lon1) {
        double lon2, lat2;
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(address_details.getLongitude());
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(address_details.getLatitude());

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2);

        double c = 2 * Math.asin(Math.sqrt(a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;

        // calculate the result
        deliveryDistance = (c * r);
    }

    public static class ItemsBean implements Parcelable {
        /**
         * id : 106
         * product_name : Mask
         * created : 2021-07-10T18:20:14.791272+05:30
         * last_updated : 2021-07-10T18:20:14.791301+05:30
         * item_name : standard
         * amount : 120
         * security_deposit : 0
         * quantity : 2
         * item : 33
         */

        private int id;
        private String product_name;
        private String created;
        private String last_updated;
        private String item_name;
        private int amount;
        private int security_deposit;
        private int quantity;
        private int item;

        public ItemsBean(ItemsBean itemsBean){
            id = itemsBean.id;
            product_name = itemsBean.product_name;
            created = itemsBean.created;
            last_updated = itemsBean.last_updated;
            item_name = itemsBean.item_name;
            amount = itemsBean.amount;
            security_deposit = itemsBean.security_deposit;
            quantity = itemsBean.quantity;
            item = itemsBean.item;
        }

        protected ItemsBean(Parcel in) {
            id = in.readInt();
            product_name = in.readString();
            created = in.readString();
            last_updated = in.readString();
            item_name = in.readString();
            amount = in.readInt();
            security_deposit = in.readInt();
            quantity = in.readInt();
            item = in.readInt();
        }

        public static final Creator<ItemsBean> CREATOR = new Creator<ItemsBean>() {
            @Override
            public ItemsBean createFromParcel(Parcel in) {
                return new ItemsBean(in);
            }

            @Override
            public ItemsBean[] newArray(int size) {
                return new ItemsBean[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getItem_name() {
            return item_name;
        }

        public void setItem_name(String item_name) {
            this.item_name = item_name;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getSecurity_deposit() {
            return security_deposit;
        }

        public void setSecurity_deposit(int security_deposit) {
            this.security_deposit = security_deposit;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public int getItem() {
            return item;
        }

        public void setItem(int item) {
            this.item = item;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(product_name);
            dest.writeString(created);
            dest.writeString(last_updated);
            dest.writeString(item_name);
            dest.writeInt(amount);
            dest.writeInt(security_deposit);
            dest.writeInt(quantity);
            dest.writeInt(item);
        }
    }
}
