package modal;

public class PaymentResponceTokenModal {

    /**
     * status : OK
     * message : Token generated
     * cftoken : QM9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.6f9JiYklDOlRDOwQWOjBjNiojI0xWYz9lIsITN4AzM0YjM2EjOiAHelJCLiIlTJJiOik3YuVmcyV3QyVGZy9mIsEjOiQnb19WbBJXZkJ3biwiIxADMwIXZkJ3TiojIklkclRmcvJye.FAfg2o-Sq2eLDnb-f6gezJ8GRltj3Yc8NKhUOw41TWnn_SL0acDXyiuNvgbYglcUwn
     */

    private String status;
    private String message;
    private String cftoken;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCftoken() {
        return cftoken;
    }

    public void setCftoken(String cftoken) {
        this.cftoken = cftoken;
    }
}
