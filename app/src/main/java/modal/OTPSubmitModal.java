package modal;

import com.google.gson.annotations.SerializedName;

public class OTPSubmitModal {


    /**
     * key : 54f64fbbfc22cec3f4081c31800a59d92de37d2b
     * user : CUST_8619368171
     * is_new_user : false
     * customer_details : {"id":82,"created":"2021-06-30T23:06:00.784908+05:30","last_updated":"2021-07-08T09:00:15.036057+05:30","name":"DEEPENDRA SINGH","mobile":"8619368171","slug":"deependra-singh","dob":"2021-06-30","gender":"Female","aadhar_number":"556757677726","medical_report":"/media/upload/files/IMG20210707190456_00_2FEgT2x.jpg","prescription_document":"/media/upload/files/IMG-20210705-WA0001_skvbpCh.jpg","user":2,"default_address":81,"prescribe_by":86,"default_dealer":2}
     */

    private String key;
    private String user;
    private boolean is_new_user;
    @SerializedName("deliveryboy_details")
    private CustomerDetailsBean customer_details;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isIs_new_user() {
        return is_new_user;
    }

    public void setIs_new_user(boolean is_new_user) {
        this.is_new_user = is_new_user;
    }

    public CustomerDetailsBean getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(CustomerDetailsBean customer_details) {
        this.customer_details = customer_details;
    }

    public static class CustomerDetailsBean {
        /**
         * id : 82
         * created : 2021-06-30T23:06:00.784908+05:30
         * last_updated : 2021-07-08T09:00:15.036057+05:30
         * name : DEEPENDRA SINGH
         * mobile : 8619368171
         * slug : deependra-singh
         * dob : 2021-06-30
         * gender : Female
         * aadhar_number : 556757677726
         * medical_report : /media/upload/files/IMG20210707190456_00_2FEgT2x.jpg
         * prescription_document : /media/upload/files/IMG-20210705-WA0001_skvbpCh.jpg
         * user : 2
         * default_address : 81
         * prescribe_by : 86
         * default_dealer : 2
         */

        private int id;
        private String created;
        private String last_updated;
        private String name;
        private String mobile;
        private String slug;
        private String dob;
        private String gender;
        private String aadhar_number;
        private String medical_report;
        private String prescription_document;
        private int user;
        private int default_address;
        private int prescribe_by;
        private int default_dealer;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getLast_updated() {
            return last_updated;
        }

        public void setLast_updated(String last_updated) {
            this.last_updated = last_updated;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAadhar_number() {
            return aadhar_number;
        }

        public void setAadhar_number(String aadhar_number) {
            this.aadhar_number = aadhar_number;
        }

        public String getMedical_report() {
            return medical_report;
        }

        public void setMedical_report(String medical_report) {
            this.medical_report = medical_report;
        }

        public String getPrescription_document() {
            return prescription_document;
        }

        public void setPrescription_document(String prescription_document) {
            this.prescription_document = prescription_document;
        }

        public int getUser() {
            return user;
        }

        public void setUser(int user) {
            this.user = user;
        }

        public int getDefault_address() {
            return default_address;
        }

        public void setDefault_address(int default_address) {
            this.default_address = default_address;
        }

        public int getPrescribe_by() {
            return prescribe_by;
        }

        public void setPrescribe_by(int prescribe_by) {
            this.prescribe_by = prescribe_by;
        }

        public int getDefault_dealer() {
            return default_dealer;
        }

        public void setDefault_dealer(int default_dealer) {
            this.default_dealer = default_dealer;
        }
    }
}
