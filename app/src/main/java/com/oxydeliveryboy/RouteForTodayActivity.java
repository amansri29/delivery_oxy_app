package com.oxydeliveryboy;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.JsonObject;
import com.oxydeliveryboy.databinding.ActivityRouteForTodayBinding;
import com.oxydeliveryboy.databinding.CustomDialogBinding;
import com.oxydeliveryboy.databinding.OtpBottonSheetLayoutBinding;
import com.oxydeliveryboy.databinding.OxygenCylinderNumberLayoutBinding;
import com.oxydeliveryboy.retrofit.APIClientMain;
import com.oxydeliveryboy.retrofit.APIInterface;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import adapters.HomeAllOrderProductAdapter;
import im.delight.android.location.SimpleLocation;
import modal.AllOrdersGetModal;
import modal.OrderDeliverd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteForTodayActivity extends AppCompatActivity implements OnMapReadyCallback {
    ActivityRouteForTodayBinding binding;
    private Context context;
    String destilat, destilang, sourseLat, sourseLong;
    private GoogleMap myMap;
    String totalAmount, token, otp, cylinderNumber;
    /*List<AllOrdersGetModal> items;*/
    private APIInterface apiInterface;
    StorageUtils storageUtils;
    private static final String TAG = "RouteForTodayActivity";
    private SimpleLocation simpleLocation;
    String deliveryCode;

    Double lat, longitude;
    private String orderType;
    private boolean cylinderContained;
    private ProgressDialog progressDialog;
    private String status, paymentMode;
    private List<AllOrdersGetModal.ItemsBean> items, itemWithSecurity;
    private int totalSecurityAmount;
    private boolean isPickupOrder = false;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_route_for_today);
        context = RouteForTodayActivity.this;
        storageUtils = new StorageUtils();
        simpleLocation = new SimpleLocation(this);
        // Get the SupportMapFragment and request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//        items = (List<AllOrdersGetModal.ItemsBean>) getIntent().getSerializableExtra("allItems");
        totalAmount = getIntent().getStringExtra("totalAmount");
        lat = getIntent().getDoubleExtra("lat", 0.0);
        longitude = getIntent().getDoubleExtra("long", 0.0);
        orderType = getIntent().getStringExtra("order type");
        cylinderContained = getIntent().getBooleanExtra("contains cylinder", false);
        items = getIntent().getParcelableArrayListExtra("items");
        itemWithSecurity = getIntent().getParcelableArrayListExtra("itemsWithSecurity");
        status = getIntent().getStringExtra("status");
        totalSecurityAmount = getIntent().getIntExtra("securityAmount", 0);
        deliveryCode = getIntent().getStringExtra("code");
//        items = (List<AllOrdersGetModal>) getIntent().getSerializableExtra("items");


        paymentMode = getIntent().getStringExtra("payment mode");

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Loading.....");
        progressDialog.setCancelable(false);



        token = storageUtils.getDetails(context, StringUtils.UserToken);

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.tvGetDeliveryRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBottomSheet();
            }
        });
        isPickupOrder = status.equals("Return Requested");
        if (isPickupOrder) {
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    itemWithSecurity, true);
            binding.itemsListRv.setHasFixedSize(true);
            binding.itemsListRv.setLayoutManager(new LinearLayoutManager(this));
            binding.itemsListRv.setAdapter(homeAllOrderProductAdapter);

            binding.tvGetDeliveryRoute.setText("Complete Pick Up");
            binding.tvAllItems.setText("Pickup Items");
            if (paymentMode.equals("Online")) {
                binding.orderType.setTextColor(ContextCompat.getColor(this, R.color.color44D639));
            }
            binding.orderType.setText("Amount to be refunded");
            binding.tvTotalAmount.setText("₹ " + totalSecurityAmount);
        } else {
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    items, false);
            binding.itemsListRv.setHasFixedSize(true);
            binding.itemsListRv.setLayoutManager(new LinearLayoutManager(this));
            binding.itemsListRv.setAdapter(homeAllOrderProductAdapter);
            if (paymentMode.equals("Online")) {
                binding.orderType.setText("Prepaid");
                binding.orderType.setTextColor(ContextCompat.getColor(this, R.color.color44D639));
            }

            binding.tvTotalAmount.setText("₹ " + totalAmount);
        }


    }

    private void setCameraWithCoordinationBounds(List<com.akexorcist.googledirection.model.Route> route,
                                                 GoogleMap googleMap) {
        LatLng southwest = route.get(0).getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.get(route.size() - 1).getBound().getNortheastCoordination()
                .getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    public void openBottomSheet() {
        final OtpBottonSheetLayoutBinding binding =
                DataBindingUtil.bind(getLayoutInflater().
                        inflate(R.layout.otp_botton_sheet_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context,
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        binding.tvVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otp = binding.editOtp.getText().toString().trim();
                if (otp.isEmpty()) {
                    Toast.makeText(RouteForTodayActivity.this, "Please enter the Otp", Toast.LENGTH_SHORT).show();
                } else if (!otp.equals(deliveryCode)) {
                    Toast.makeText(RouteForTodayActivity.this, "Please enter the valid Otp!", Toast.LENGTH_SHORT).show();
                } else {
                    bottomSheetDialog.dismiss();
                    if (orderType.equalsIgnoreCase("Refill") || isPickupOrder)
                        openCylinderNumberBottomSheet();
                    else {
                        if (!progressDialog.isShowing()) {
                            progressDialog.show();
                        }
                        orderDeliverd(false);
                    }
                }
            }
        });

        bottomSheetDialog.show();

    }

    private void openCylinderNumberBottomSheet() {
        final OxygenCylinderNumberLayoutBinding binding =
                DataBindingUtil.bind(getLayoutInflater().
                        inflate(R.layout.oxygen_cylinder_number_layout,
                                null));
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context,
                R.style.BottomSheetDialog);

        assert binding != null;
        bottomSheetDialog.setContentView(binding.getRoot());

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cylinderNumber = binding.editCylinderNumber.getText().toString().trim();
                if (cylinderNumber.isEmpty()) {
                    Toast.makeText(RouteForTodayActivity.this, "Please enter the cylinder number!", Toast.LENGTH_SHORT).show();
                } else {
                    bottomSheetDialog.dismiss();
                    if(!progressDialog.isShowing()){
                        progressDialog.show();
                    }
                    orderDeliverd(true);
                }
            }
        });

        bottomSheetDialog.show();

    }

    private void orderDeliverd(boolean cylinderNumberRequired) {
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("otp", otp);
        if(cylinderNumberRequired)
            jsonObject.addProperty("cylender_number", cylinderNumber);
        else
            jsonObject.addProperty("cylender_number", "NA");
        String Token = "Token " + token;
        Call<OrderDeliverd> call = apiInterface.orderDeliverd(Token, jsonObject);
        call.enqueue(new Callback<OrderDeliverd>() {
            @Override
            public void onResponse(@NotNull Call<OrderDeliverd> call,
                                   @NotNull Response<OrderDeliverd> response) {
                progressDialog.dismiss();
                OrderDeliverd allDealersModals = response.body();
                if (allDealersModals != null) {
                    if (isPickupOrder) {
                        Toast.makeText(RouteForTodayActivity.this, "Item Pick up Successful", Toast.LENGTH_SHORT).show();
                        if (!paymentMode.equals("Online")) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(RouteForTodayActivity.this);
                            CustomDialogBinding binding = CustomDialogBinding.inflate(getLayoutInflater());
                            builder1.setView(binding.getRoot());
                            binding.message.setText("Please Provide Refund of ₹" + totalSecurityAmount + " to the Contact Person...");
                            builder1.setCancelable(false);
                            AlertDialog alert11 = builder1.create();
                            binding.llMarkDelivery.setOnClickListener(v -> {
                                if (alert11 != null) {
                                    alert11.dismiss();
                                }
                                finish();
                            });


                            alert11.show();
                        }
                        else{
                            Toast.makeText(RouteForTodayActivity.this, "Item Pick up Successful", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {
                        Toast.makeText(RouteForTodayActivity.this, "Order Delivered successfully!", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                } else {
                    Toast.makeText(context, "Error occurred!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderDeliverd> call, @NotNull Throwable t) {
                progressDialog.dismiss();
                Log.e("Faliour", t.getMessage());
                Toast.makeText(context, "OTP is not correct", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        myMap = googleMap;
        if (checkLocationPermissions()) {
            Log.d(TAG, "onMapReady: fetch Current Location");
            requestNewLocationData();
        } else {
            requestLocationPermissions();
        }
    }

    private void requestLocationPermissions() {
        String[] permissionSet = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
        ActivityCompat.requestPermissions(this, permissionSet, 20);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(checkLocationPermissions()){
            requestNewLocationData();
        }
        else{
            Toast.makeText(this, "Please allow location permission to use the app",Toast.LENGTH_SHORT).show();
        }
    }

    private void plotPolyLine(LatLng positionFrom) {
        LatLng positionTo = new LatLng(lat, longitude);

        Log.d(TAG, "plotPolyLine: method Called");
        GoogleDirection.withServerKey("AIzaSyAtfg4ZcsMvoVsrML7SXMgnWo2KAaUwDvI")
                .from(positionFrom)
                .to(positionTo)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(@Nullable Direction direction) {
                        if (direction.isOK()) {
                            List<Route> route = direction.getRouteList();
                            myMap.addMarker(new MarkerOptions().position(positionFrom).title("Your Location"));
                            myMap.addMarker(new MarkerOptions().position(positionTo).title("Deliver Here"));

                            ArrayList<LatLng> directionPositionList =
                                    route.get(0).getLegList().get(0).getDirectionPoint();
                            myMap.addPolyline(DirectionConverter.createPolyline(
                                    context,
                                    directionPositionList,
                                    3,
                                    Color.BLACK
                            ));
                            setCameraWithCoordinationBounds(route, myMap);
                            myMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionTo, 12));
                            Log.d(TAG, "onDirectionSuccess: polyline draw success");
                        } else {
                            // Do something
                        }
                    }

                    @Override
                    public void onDirectionFailure(@NonNull Throwable t) {
                        Log.d(TAG, "onDirectionFailure: ");
                    }
                });
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private Boolean checkLocationPermissions() {
        return ContextCompat.checkSelfPermission(RouteForTodayActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(RouteForTodayActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5);
        mLocationRequest.setFastestInterval(0);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location mLastLocation = locationResult.getLastLocation();
                Log.d(TAG, "onLocationResult: location fetched");
                plotPolyLine(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                mFusedLocationClient.removeLocationUpdates(this);
            }
        }, Looper.myLooper());
    }

}