package com.oxydeliveryboy.retrofit;

import android.content.Context;

import com.google.gson.JsonObject;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import org.jetbrains.annotations.NotNull;

import modal.OTPSendModal;
import modal.OTPSubmitModal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiController {
    private APIInterface apiInterface;
    private ApiResponseListener apiResponseListener;
    //    private DealerResponceListener dealerResponceListener;
    StorageUtils storageUtils;
    private static final String TAG = "ApiController";

    public ApiController(ApiResponseListener apiResponseListener) {
        this.apiResponseListener = apiResponseListener;
        if (APIClientMain.getClient() != null) {
            apiInterface = APIClientMain.getClient().create(APIInterface.class);
        }
    }

    public void getPhoneNumberOtpSend(String number) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", "DEALBOY_" + number);
        Call<OTPSendModal> call = apiInterface.getPhonenumberOtp(jsonObject);
        call.enqueue(new Callback<OTPSendModal>() {
            @Override
            public void onResponse(@NotNull Call<OTPSendModal> call,
                                   @NotNull Response<OTPSendModal> response) {
                OTPSendModal detailsModal = response.body();
                if (detailsModal != null) {
                    if (detailsModal.getMsg().equals("OTP send")) {
                        apiResponseListener.onSuccess(StringUtils.PhoneNumber, detailsModal.getMsg());
                    } else {
                        apiResponseListener.onFailure(detailsModal.getMsg());
                    }
                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OTPSendModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }

    public void getPhoneNumberOtpSubmit(String number, String otp, StorageUtils storageUtils, Context context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username", "CUST_" + number);
        jsonObject.addProperty("password", otp);
        Call<OTPSubmitModal> call = apiInterface.submitPhonenumberOtp(jsonObject);
        call.enqueue(new Callback<OTPSubmitModal>() {
            @Override
            public void onResponse(@NotNull Call<OTPSubmitModal> call,
                                   @NotNull Response<OTPSubmitModal> response) {
                OTPSubmitModal detailsModal = response.body();
                if (detailsModal != null) {
                    storageUtils.setDetails(context, StringUtils.NewUser, String.valueOf(detailsModal.isIs_new_user()));
                    apiResponseListener.onSuccess(String.valueOf(detailsModal.isIs_new_user()), String.valueOf(detailsModal.getKey()));

                } else {
                    apiResponseListener.onError("Server Error ReStart App");
                }
            }

            @Override
            public void onFailure(@NotNull Call<OTPSubmitModal> call, @NotNull Throwable t) {
                apiResponseListener.onError(t.getMessage());
            }
        });
    }
//    9414553801
}
