package com.oxydeliveryboy.retrofit;

/**
 * @author bhagyshalee gour
 * used this interface for api response handle
 */

public interface ApiResponseListener {
    void onSuccess(String tag, String superClassCastBean);

    void onFailure(String msg);

    void onError(String msg);

    void onPerformCode(int code);
}
