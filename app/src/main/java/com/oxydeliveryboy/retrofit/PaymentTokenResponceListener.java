package com.oxydeliveryboy.retrofit;

import modal.PaymentResponceTokenModal;

public interface PaymentTokenResponceListener {
    void onSuccess(String beanTag, PaymentResponceTokenModal superClassCastBean);

    void onFailure(String msg);

    void onError(String msg);

    void onPerformCode(int code);
}
