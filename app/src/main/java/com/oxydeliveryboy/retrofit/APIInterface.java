package com.oxydeliveryboy.retrofit;

import com.google.gson.JsonObject;

import java.util.List;

import modal.AllOrdersGetModal;
import modal.OTPSendModal;
import modal.OTPSubmitModal;
import modal.OrderDeliverd;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {
//    @POST("order")
//    Call<PaymentResponceTokenModal> getPaymentToken(@Header("Content-Type") String deviceReg,
//                                                    @Header("x-client-id") String deviceInfo,
//                                                    @Header("x-client-secret") String deviceType, @Body JsonObject jsonObject);

    @POST("send-otp/")
    Call<OTPSendModal> getPhonenumberOtp(@Body JsonObject jsonObject);

    @POST("login/")
    Call<OTPSubmitModal> submitPhonenumberOtp(@Body JsonObject jsonObject);


    @GET("delivery/myorders/")
    Call<List<AllOrdersGetModal>> getAllorders(@Header("Authorization") String token);

    @GET("delivery/myorders/")
    Call<List<AllOrdersGetModal>> getParticularDateOrder(@Header("Authorization") String token,
                                                         @Query("expected_delivery_date__lte") String sDate,
                                                         @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/myorders/")
    Call<List<AllOrdersGetModal>> getOrdersAtParticularDay(@Header("Authorization") String token,
                                                           @Query("expected_delivery_date__exact") String Date
    );

    @GET("delivery/despetch/")
    Call<List<AllOrdersGetModal>> getAllDishpatches(@Header("Authorization") String token,
                                                    @Query("expected_delivery_date__lte") String sDate,
                                                    @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/despetch/")
    Call<List<AllOrdersGetModal>> getLastMonthDishpatches(@Header("Authorization") String token,
                                                          @Query("expected_delivery_date__lte") String sDate,
                                                          @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/despetch/")
    Call<List<AllOrdersGetModal>> getLastSevenDaysDishpatches(@Header("Authorization") String token,
                                                              @Query("expected_delivery_date__lte") String sDate,
                                                              @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/despetch/")
    Call<List<AllOrdersGetModal>> getRandeDateDishpatches(@Header("Authorization") String token,
                                                          @Query("expected_delivery_date__lte") String sDate,
                                                          @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/cashcollector/")
    Call<List<AllOrdersGetModal>> getAllCashCollector(@Header("Authorization") String token,
                                                      @Query("expected_delivery_date__lte") String sDate,
                                                      @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/cashcollector/")
    Call<List<AllOrdersGetModal>> getLastSevenDaysCashCollector(@Header("Authorization") String token,
                                                                @Query("expected_delivery_date__lte") String sDate,
                                                                @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/cashcollector/")
    Call<List<AllOrdersGetModal>> getLastmonthCashCollector(@Header("Authorization") String token,
                                                            @Query("expected_delivery_date__lte") String sDate,
                                                            @Query("expected_delivery_date__gte") String eDate);

    @GET("delivery/cashcollector/")
    Call<List<AllOrdersGetModal>> getRangDateCashCollector(@Header("Authorization") String token,
                                                           @Query("expected_delivery_date__lte") String sDate,
                                                           @Query("expected_delivery_date__gte") String eDate);

    @POST("order/")
    Call<AllOrdersGetModal> placedOrder(@Header("Authorization") String token
            , @Body Order order);

    @POST("deliveryOtp")
    Call<OrderDeliverd> orderDeliverd(@Header("Authorization") String token
            , @Body JsonObject order);

    @PATCH("order/{orderId}/")
    Call<AllOrdersGetModal> updateCylinderNumber(@Header("Authorization") String token
            , @Body JsonObject orderDate, @Path("orderId") int orderId);

}
