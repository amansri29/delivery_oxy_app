package com.oxydeliveryboy.retrofit;

import java.util.List;

public class Order {
    //    {
//        "items": [{"item": 33, "quantity": 2},{"item": 34, "quantity": 2}],
//        "total_amount": 150.0,
//            "expected_delivery_date": "2020-10-10",
//            "delivery_address": null,
//            "payment_mode": "Cash on delivery",
//            "status": "Payment Pending",
//            "order_type":"New",
//            "repeat_order_days" : 0,
//            "repeat_till" : "2020-10-12"
//    }

    double total_amount;
    String expected_delivery_date;
    int delivery_address;
    String payment_mode;
    String status;
    String order_type;
    int repeat_order_days;
    String repeat_till;
    int payment_id;
    int order_dealer;
    List<OrderItem> items;

    public Order()
    {

    }

    public Order(double total_amount, String expected_delivery_date, int delivery_address,
                 String payment_mode, String status, String order_type, int repeat_order_days,
                 String repeat_till, int payment_id, List<OrderItem> items) {
        this.total_amount = total_amount;
        this.expected_delivery_date = expected_delivery_date;
        this.delivery_address = delivery_address;
        this.payment_mode = payment_mode;
        this.status = status;
        this.order_type = order_type;
        this.repeat_order_days = repeat_order_days;
        this.repeat_till = repeat_till;
        this.payment_id = payment_id;
        this.items = items;
    }

    //
//    public Order(double total_amount, String expected_delivery_date, int delivery_address,
//                 String payment_mode, String status, String order_type,
//                 int repeat_order_days, String repeat_till, List<OrderItem> items) {
//        this.total_amount = total_amount;
//        this.expected_delivery_date = expected_delivery_date;
//        this.delivery_address = delivery_address;
//        this.payment_mode = payment_mode;
//        this.status = status;
//        this.order_type = order_type;
//        this.repeat_order_days = repeat_order_days;
//        this.repeat_till = repeat_till;
//        this.items = items;
//    }


    public void setOrder_dealer(int order_dealer) {
        this.order_dealer = order_dealer;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public int getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(int delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public int getRepeat_order_days() {
        return repeat_order_days;
    }

    public void setRepeat_order_days(int repeat_order_days) {
        this.repeat_order_days = repeat_order_days;
    }

    public String getRepeat_till() {
        return repeat_till;
    }

    public void setRepeat_till(String repeat_till) {
        this.repeat_till = repeat_till;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

}
