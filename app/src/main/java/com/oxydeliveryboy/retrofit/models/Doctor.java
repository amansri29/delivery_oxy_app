package com.oxydeliveryboy.retrofit.models;

public class Doctor {
    private String name;
    private String mobile;
    private int id;

    public Doctor(String name, String mobile) {
        this.name = name;
        this.mobile = mobile;
    }


    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public int getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
