package com.oxydeliveryboy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.oxydeliveryboy.databinding.ActivityMainBinding;
import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import fragments.CashCollectionFragment;
import fragments.DispatchFragment;
import fragments.HomeFragment;
import fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityMainBinding binding;
    Context context;
    StorageUtils storageUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        context = MainActivity.this;
        storageUtils = new StorageUtils();

        binding.imgNavigation.setOnClickListener(this);
        binding.navLayout.navHome.setOnClickListener(this);
        binding.navLayout.navDispatches.setOnClickListener(this);
        binding.navLayout.CashcollectionReportNav.setOnClickListener(this);
        binding.navLayout.navLogout.setOnClickListener(this);
        binding.navLayout.imgClose.setOnClickListener(this);
        binding.navLayout.tvEditProfile.setOnClickListener(this);

        loadFragment(new HomeFragment());
        binding.navLayout.tvUserName.setText(storageUtils.getDetails(this, StringUtils.UserName));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgNavigation:
                isDrawerOpen();
                break;
            case R.id.navHome:
                isDrawerOpen();
                binding.tvNavHeading.setText("Home");
                loadFragment(new HomeFragment());
                break;
            case R.id.CashcollectionReportNav:
                isDrawerOpen();
                binding.tvNavHeading.setText("Cash collection");
                loadFragment(new CashCollectionFragment());
                break;
            case R.id.navDispatches:
                isDrawerOpen();
                binding.tvNavHeading.setText("Dispatches");
                loadFragment(new DispatchFragment());
                break;

            case R.id.tvEditProfile:
                isDrawerOpen();
                binding.tvNavHeading.setText("Profile");
                loadFragment(new ProfileFragment());
                break;
            case R.id.navLogout:
                isDrawerOpen();
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle("Logout ?");
                builder1.setMessage("Are you sure want to logout?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                storageUtils.clearSharedPrefrence(MainActivity.this);
                                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
                break;
            case R.id.imgClose:
                isDrawerOpen();
                break;
        }
    }

    public void isDrawerOpen() {
        if (binding.drower.isDrawerOpen(Gravity.LEFT)) {
            binding.drower.closeDrawer(Gravity.LEFT);
        } else {
            binding.drower.openDrawer(Gravity.LEFT);
        }
    }

    public void loadFragment(Fragment fragmnet) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragmnet);
        fragmentTransaction.commit();
    }
}