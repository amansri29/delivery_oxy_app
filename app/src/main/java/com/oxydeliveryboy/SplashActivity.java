package com.oxydeliveryboy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.oxydeliveryboy.sharedprefrence.StorageUtils;
import com.oxydeliveryboy.utils.StringUtils;

import activities.PhoneNumberActivity;

public class SplashActivity extends AppCompatActivity {
    String isLogin;
    StorageUtils storageUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        storageUtils = new StorageUtils();
        isLogin = storageUtils.getDetails(SplashActivity.this, StringUtils.IsLogin);

        if (isLogin.equals("Yes")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }, 2000);

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, PhoneNumberActivity.class));
                    finish();
                }
            }, 2000);

        }

    }
}