package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.DispatchesRecyclerFormatBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import modal.AllOrdersGetModal;

public class DispatchesFragmentAdapter extends RecyclerView.Adapter<DispatchesFragmentAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal> allDealersModals;
    SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-mm-dd");
    SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

    public DispatchesFragmentAdapter(Context context, List<AllOrdersGetModal> allDealersModals) {
        this.context = context;
        this.allDealersModals = allDealersModals;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        DispatchesRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.dispatches_recycler_format, parent, false));
        DispatchesFragmentAdapter.ViewHolder viewHolder = new DispatchesFragmentAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvUserName.setText(allDealersModals.get(position).getCustomer_name());
        holder.binding.tvRefil.setText(allDealersModals.get(position).getOrder_type());
        holder.binding.tvOrderId.setText(String.valueOf(allDealersModals.get(position).getId()));

        String expectedPrettyDate = "";
        try{
            Date d = serverDateFormat.parse(allDealersModals.get(position).getExpected_delivery_date());
            expectedPrettyDate = prettyDateFormat.format(d);
        }
        catch (Exception e){

        }
        holder.binding.tvDate.setText(expectedPrettyDate);

        DispatchItemsAdapter dispatchItemsAdapter = new DispatchItemsAdapter(context,
                allDealersModals.get(position).getItems());
        holder.binding.dishpatchItemRecycler.setHasFixedSize(true);
        holder.binding.dishpatchItemRecycler.setAdapter(dispatchItemsAdapter);

    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        DispatchesRecyclerFormatBinding binding;

        public ViewHolder(@NonNull DispatchesRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
