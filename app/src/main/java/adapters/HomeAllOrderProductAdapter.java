package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.HomeOrderAllproductFormatBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import modal.AllOrdersGetModal;

public class HomeAllOrderProductAdapter extends RecyclerView.Adapter<HomeAllOrderProductAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal.ItemsBean> items;
    List<AllOrdersGetModal> allDealersModals;
    private boolean isPickup = false;

    public HomeAllOrderProductAdapter(Context context, List<AllOrdersGetModal.ItemsBean> items, boolean isPickup) {
        this.context = context;
        this.items = items;
        this.isPickup = isPickup;
    }

    public HomeAllOrderProductAdapter(Context context, List<AllOrdersGetModal.ItemsBean> items,
                                      List<AllOrdersGetModal> allDealersModals, boolean isPickup) {
        this.context = context;
        this.items = items;
        this.allDealersModals = allDealersModals;
        this.isPickup = isPickup;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        HomeOrderAllproductFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.home_order_allproduct_format, parent, false));
        HomeAllOrderProductAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String itemType = items.get(position).getItem_name();
        int itemQuantity = items.get(position).getQuantity();
        String itemName = String.valueOf(items.get(position).getProduct_name());

        if(!isPickup) {
            if (itemType.equals("standard")) {
                itemType = "S";
                holder.binding.cylenderNameQtyType.setText(itemName + "( " + itemType + " )");
                holder.binding.orderAmount.setText(itemQuantity + " Unit ");
                int itemTotalAmount = items.get(position).getAmount() * itemQuantity;
                holder.binding.cylenderDeliveryDate.setText("₹ " + itemTotalAmount);
            } else {
                holder.binding.cylenderNameQtyType.setText(itemName + "( " + itemType + " )");
                holder.binding.orderAmount.setText(itemQuantity + " Unit ");
                int itemTotalAmount = items.get(position).getAmount() * itemQuantity;
                holder.binding.cylenderDeliveryDate.setText("₹ " + itemTotalAmount);
            }
        }
        else{
            holder.binding.cylenderNameQtyType.setText(itemName + "( " + itemType + " )");
            holder.binding.orderAmount.setText(itemQuantity + " Unit ");
            int itemTotalAmount = items.get(position).getSecurity_deposit() * itemQuantity;
            holder.binding.cylenderDeliveryDate.setText("₹ " + itemTotalAmount);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        HomeOrderAllproductFormatBinding binding;

        public ViewHolder(@NonNull HomeOrderAllproductFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
