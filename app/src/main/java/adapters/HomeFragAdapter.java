package adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.RouteForTodayActivity;
import com.oxydeliveryboy.databinding.HomeRecyclerFormatBinding;

import java.util.List;

import listeners.DealersCallListeners;
import modal.AllOrdersGetModal;

public class HomeFragAdapter extends RecyclerView.Adapter<HomeFragAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal> dealersModalArrayList;
    DealersCallListeners dealersCallListeners;

    public HomeFragAdapter(Context context, List<AllOrdersGetModal> dealersModalArrayList,
                           DealersCallListeners dealersCallListeners) {
        this.context = context;
        this.dealersModalArrayList = dealersModalArrayList;
        this.dealersCallListeners = dealersCallListeners;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        HomeRecyclerFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.home_recycler_format, parent, false));
        HomeFragAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AllOrdersGetModal order = dealersModalArrayList.get(position);

        holder.binding.cylenderOrderId.setText(String.valueOf(dealersModalArrayList.get(position).getId()));
        holder.binding.userName.setText(dealersModalArrayList.get(position).getCustomer_name());
//        holder.binding.cylindersQty.setText(dealersModalArrayList.get(position).getCylinderQty());
        if(order.getStatus().equals("Return Requested")){
            holder.binding.tvAddress.setText("PICKUP ADDRESS");
            holder.binding.tvAllItems.setText("PICKUP ITEMS");
            holder.binding.tvPickupOrder.setVisibility(View.VISIBLE);
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    dealersModalArrayList.get(position).getItemWithSecurity(), dealersModalArrayList,true);
            holder.binding.allProductRecycler.setHasFixedSize(true);
            holder.binding.allProductRecycler.setAdapter(homeAllOrderProductAdapter);
            holder.binding.cylindersPrepaid.setVisibility(View.GONE);
            holder.binding.tvPrepaired.setVisibility(View.GONE);
            holder.binding.tvMarkDelivery.setText("Mark Pickup");
            holder.binding.llMarkDelivery.setBackgroundColor(ContextCompat.getColor(context,R.color.colorFE534C));

        }
        else{
            holder.binding.tvAddress.setText("DELIVERY ADDRESS");
            holder.binding.tvAllItems.setText("DELIVER ITEMS");
            holder.binding.tvPickupOrder.setVisibility(View.GONE);
            HomeAllOrderProductAdapter homeAllOrderProductAdapter = new HomeAllOrderProductAdapter(context,
                    dealersModalArrayList.get(position).getItems(), dealersModalArrayList, false);
            holder.binding.allProductRecycler.setHasFixedSize(true);
            holder.binding.allProductRecycler.setAdapter(homeAllOrderProductAdapter);
            holder.binding.cylindersPrepaid.setVisibility(View.VISIBLE);
            holder.binding.tvPrepaired.setVisibility(View.VISIBLE);
            holder.binding.tvMarkDelivery.setText("Mark Delivery");
            holder.binding.llMarkDelivery.setBackgroundColor(ContextCompat.getColor(context,R.color.color3B6EFF));
        }
        holder.binding.deliveryAddress.setText(dealersModalArrayList.get(position).getAddress_details().getBasic_address());

        String totalAmount = String.valueOf(dealersModalArrayList.get(position).getTotal_amount());
        String paymentMode = dealersModalArrayList.get(position).getPayment_mode();

        if (paymentMode.equals("Cash on delivery")) {
            holder.binding.cylindersPrepaid.setText("₹" + totalAmount + " to be taken");
            holder.binding.cylindersPrepaid.setTextColor(context.getResources().getColor(R.color.colorFE534C));
        } else {
            holder.binding.cylindersPrepaid.setText("Prepaid");
            holder.binding.cylindersPrepaid.setTextColor(context.getResources().getColor(R.color.color44D639));
        }

        holder.binding.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dealersCallListeners.dealersCall(position);
            }
        });
        holder.binding.llMarkDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cylinderContained = false;
                for(AllOrdersGetModal.ItemsBean item : order.getItems()){
                    if(item.getItem_name().contains("Cylinder")){
                        cylinderContained = true;
                        break;
                    }
                }
                context.startActivity(new Intent(context, RouteForTodayActivity.class)
                        .putExtra("totalAmount", totalAmount)
                        .putExtra("lat", order.getAddress_details().getLatitude())
                        .putExtra("long", order.getAddress_details().getLongitude())
                        .putExtra("payment mode", order.getPayment_mode())
                        .putExtra("order type", order.getOrder_type())
                        .putExtra("contains cylinder", cylinderContained)
                        .putExtra("status", order.getStatus())
                        .putExtra("securityAmount", order.getTotalSecurityDeposit())
                        .putExtra("code",order.getDelivery_code())
                        .putParcelableArrayListExtra("items", order.getItems())
                        .putParcelableArrayListExtra("itemsWithSecurity", order.getItemWithSecurity()));



                        /*.

                                putExtra("allItems",
                                        (Serializable) dealersModalArrayList.get(position).getItems()*/
                ;
            }
        });




        holder.binding.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + order.getCustomer_contact()));
                    context.startActivity(intent);
                }
                else{
                    requestCallPermission();
                    Toast.makeText(context, "Please Provide this Permission to provide tap to Call functionality..", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void requestCallPermission() {
        String[] permissionArray = {Manifest.permission.CALL_PHONE};
        if(context instanceof Activity) {
            ActivityCompat.requestPermissions((Activity) context,
                    permissionArray,
                    10);
        }
    }

    @Override
    public int getItemCount() {
        return dealersModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        HomeRecyclerFormatBinding binding;

        public ViewHolder(@NonNull HomeRecyclerFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
