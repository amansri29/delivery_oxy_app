package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.CashCollectionFormatBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import modal.AllOrdersGetModal;

public class CashCollectionAdapter extends RecyclerView.Adapter<CashCollectionAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal> allDealersModals;
    SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat prettyDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

    public CashCollectionAdapter(Context context, List<AllOrdersGetModal> allDealersModals) {
        this.context = context;
        this.allDealersModals = allDealersModals;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CashCollectionFormatBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cash_collection_format, parent, false));
        CashCollectionAdapter.ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvUserName.setText(allDealersModals.get(position).getCustomer_name());
        holder.binding.tvOrderId.setText(String.valueOf(allDealersModals.get(position).getId()));
        String prettyExpectedDeliveryDate = "";
        String prettyReturnDate = "";
        try {
            Date d = serverDateFormat.parse(allDealersModals.get(position).getExpected_delivery_date());
            prettyExpectedDeliveryDate = prettyDateFormat.format(d);
            if(allDealersModals.get(position).getReturnDate() != null){
                Date d1 = serverDateFormat.parse(allDealersModals.get(position).getReturnDate());
                prettyReturnDate = prettyDateFormat.format(d1);
            }
        } catch (Exception e) {

        }
        holder.binding.tvDeliveryDate.setText(prettyExpectedDeliveryDate);
        if (allDealersModals.get(position).isPickupCodOrder()) {
            holder.binding.tvTotal.setText(String.valueOf("- ₹ " + allDealersModals.get(position).getTotalSecurityDeposit()));
            holder.binding.tvTotal.setTextColor(ContextCompat.getColor(context, R.color.colorFE2165));
            holder.binding.tvDeliveryDate.setText(prettyReturnDate);
            CashCollectoionItemsAdapter cashCollectoionItemsAdapter =
                    new CashCollectoionItemsAdapter(context, allDealersModals.get(position).getItemWithSecurity());
            holder.binding.cashCollectionItemRecycler.setHasFixedSize(true);
            holder.binding.cashCollectionItemRecycler.setAdapter(cashCollectoionItemsAdapter);
            holder.binding.tvDeliveryDateTitle.setText("PICKUP DATE");
        } else {
            holder.binding.tvTotal.setText(String.valueOf("₹ " + allDealersModals.get(position).getTotal_amount()));
            holder.binding.tvTotal.setTextColor(ContextCompat.getColor(context, R.color.color44D639));
            CashCollectoionItemsAdapter cashCollectoionItemsAdapter =
                    new CashCollectoionItemsAdapter(context, allDealersModals.get(position).getItems());
            holder.binding.cashCollectionItemRecycler.setHasFixedSize(true);
            holder.binding.cashCollectionItemRecycler.setAdapter(cashCollectoionItemsAdapter);
            holder.binding.tvDeliveryDateTitle.setText("DELIVERY DATE");
        }



    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CashCollectionFormatBinding binding;

        public ViewHolder(@NonNull CashCollectionFormatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
