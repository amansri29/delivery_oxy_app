package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.CashCollectionFormatBinding;
import com.oxydeliveryboy.databinding.CashCollectionRecyclerItemsBinding;

import java.util.List;

import modal.AllOrdersGetModal;

public class DispatchItemsAdapter extends RecyclerView.Adapter<DispatchItemsAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal.ItemsBean> allDealersModals;

    public DispatchItemsAdapter(Context context, List<AllOrdersGetModal.ItemsBean> allDealersModals) {
        this.context = context;
        this.allDealersModals = allDealersModals;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CashCollectionRecyclerItemsBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cash_collection_recycler_items, parent, false));
        DispatchItemsAdapter.ViewHolder viewHolder = new DispatchItemsAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvCylindersName.setText(allDealersModals.get(position).getProduct_name());
        String productQty = String.valueOf(allDealersModals.get(position).getQuantity());
        String productSize = String.valueOf(allDealersModals.get(position).getItem_name());

        if (productSize.equals("standard")) {
            String finalSize = "S";
            String finalQtySize = "(x" + productQty + finalSize + ")";
            holder.binding.cylindersQty.setText(finalQtySize);
        } else {
            String finalQtySize = "(x" + productQty + productSize + ")";
            holder.binding.cylindersQty.setText(finalQtySize);
        }
    }

    @Override
    public int getItemCount() {
        return allDealersModals.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CashCollectionRecyclerItemsBinding binding;

        public ViewHolder(@NonNull CashCollectionRecyclerItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
