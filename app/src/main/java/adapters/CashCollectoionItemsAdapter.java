package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.oxydeliveryboy.R;
import com.oxydeliveryboy.databinding.CashCollectionRecyclerItemsBinding;
import com.oxydeliveryboy.databinding.DispatchesRecyclerFormatBinding;

import java.util.List;

import modal.AllOrdersGetModal;

public class CashCollectoionItemsAdapter extends RecyclerView.Adapter<CashCollectoionItemsAdapter.ViewHolder> {
    Context context;
    List<AllOrdersGetModal.ItemsBean> items;

    public CashCollectoionItemsAdapter(Context context, List<AllOrdersGetModal.ItemsBean> items) {
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CashCollectionRecyclerItemsBinding binding = DataBindingUtil.bind(inflater.
                inflate(R.layout.cash_collection_recycler_items, parent, false));
        CashCollectoionItemsAdapter.ViewHolder viewHolder = new CashCollectoionItemsAdapter.ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.tvCylindersName.setText(items.get(position).getProduct_name());
        String productQty = String.valueOf(items.get(position).getQuantity());
        String productSize = String.valueOf(items.get(position).getItem_name());

        if (productSize.equals("standard")) {
            String finalSize = "S";
            String finalQtySize = "(x" + productQty + finalSize + ")";
            holder.binding.cylindersQty.setText(finalQtySize);
        } else {
            String finalQtySize = "(x" + productQty + productSize + ")";
            holder.binding.cylindersQty.setText(finalQtySize);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CashCollectionRecyclerItemsBinding binding;

        public ViewHolder(@NonNull CashCollectionRecyclerItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
