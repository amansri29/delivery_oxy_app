package listeners;

public interface GetSelectedDeliveryDateListener {
    public void getDeliveryDate(int date);
}
