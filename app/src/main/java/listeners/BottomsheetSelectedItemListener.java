package listeners;

public interface BottomsheetSelectedItemListener {
    void bottomsheetSelectedItem(String name);
}
