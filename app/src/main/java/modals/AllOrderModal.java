package modals;

public class AllOrderModal {
    private String orderId;
    private String userName;
    private String dealersAddress;
    private String cylinderQty;
    private String cylinderPrepaid;
    private String userNumber;

    public AllOrderModal() {
    }

    public AllOrderModal(String orderId, String userName, String dealersAddress, String cylinderQty, String cylinderPrepaid, String userNumber) {
        this.orderId = orderId;
        this.userName = userName;
        this.dealersAddress = dealersAddress;
        this.cylinderQty = cylinderQty;
        this.cylinderPrepaid = cylinderPrepaid;
        this.userNumber = userNumber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDealersAddress() {
        return dealersAddress;
    }

    public void setDealersAddress(String dealersAddress) {
        this.dealersAddress = dealersAddress;
    }

    public String getCylinderQty() {
        return cylinderQty;
    }

    public void setCylinderQty(String cylinderQty) {
        this.cylinderQty = cylinderQty;
    }

    public String getCylinderPrepaid() {
        return cylinderPrepaid;
    }

    public void setCylinderPrepaid(String cylinderPrepaid) {
        this.cylinderPrepaid = cylinderPrepaid;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }
}
